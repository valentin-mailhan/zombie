'use strict';

/*
David ->
  Camille
  julia
  marie ->
    Jean, = true *
    Pascal, = true
    Kevin = true
Julien = true -> *
  Camille = true
  julia = true
  marie = true  ->
    Jean, = true
    Pascal, = true
    Kevin = true ->
      Jean, = true
      Pascal, = true
      Kevin = true
*/

let data =
[
    {
        "name": "David",
        "isInfected": true,
        "age": 23,
        "isAlive": true,
        "variant": "ultimate",
        "isImmunised": false,
        "group": [
            {
                "name": "Camille",
                "isInfected": true,
                "age": 45,
                "isAlive": true,
                "variant": "c",
                "isImmunised": false
            },
            {
                "name": "Julia",
                "isInfected": false,
                "age": 19,
                "isAlive": true,
                "variant": "",
                "isImmunised": false
            },
            {
                "name": "Marie",
                "isInfected": true,
                "age": 16,
                "isAlive": true,
                "variant": "a",
                "isImmunised": false,
                "group": [
                    {
                        "name": "Jean",
                        "isInfected": false,
                        "age": 79,
                        "isAlive": true,
                        "variant": "",
                        "isImmunised": false
                    },
                    {
                        "name": "Pascal",
                        "isInfected": false,
                        "age": 11,
                        "isAlive": true,
                        "variant": "",
                        "isImmunised": false
                    },
                    {
                        "name": "Kévin",
                        "isInfected": false,
                        "age": 27,
                        "isAlive": true,
                        "variant": "",
                        "isImmunised": false
                    }
                ]
            }
        ]
    },
    {
        "name": "Julien",
        "isInfected": false,
        "age": 64,
        "isAlive": true,
        "variant": "",
        "isImmunised": false,
        "group": [
            {
                "name": "Bernard",
                "isInfected": false,
                "age": 31,
                "isAlive": true,
                "variant": "",
                "isImmunised": false
            },
            {
                "name": "Thomas",
                "isInfected": true,
                "age": 45,
                "isAlive": true,
                "variant": "b",
                "isImmunised": false
            },
            {
                "name": "Louise",
                "isInfected": false,
                "age": 33,
                "isAlive": true,
                "variant": "",
                "isImmunised": false,
                "group": [
                    {
                        "name": "Hugo",
                        "isInfected": false,
                        "age": 87,
                        "isAlive": true,
                        "variant": "",
                        "isImmunised": false
                    },
                    {
                        "name": "Victor",
                        "isInfected": true,
                        "age": 47,
                        "isAlive": true,
                        "variant": "32",
                        "isImmunised": false
                    },
                    {
                        "name": "Bruno",
                        "isInfected": true,
                        "age": 28,
                        "isAlive": true,
                        "variant": "ultimate",
                        "isImmunised": false
                    }
                ]
            }
        ]
    }
];

const VARIANT_A = "a";
const VARIANT_B = "b";
const VARIANT_C = "c";
const VARIANT_32 = "32";
const VARIANT_ULTIMATE = "ultimate";
const VARIANT_NONE = "";

function displayData(data) {
    for (let person of data) {
        console.log(person.name + " est " + (person.isAlive ? "en vie"  + ", a " + person.age + " ans, " + (person.isInfected ? "est infecté par le variant " + person.variant : "n'est pas infecté") + ", " + (person.isImmunised ? "est immunisé" : "n'est pas immunisé") : "mort"));
        if (person.group) {
            displayData(person.group);
        }
    }
}

function infect(person, variant) {
    person.isInfected = true;
    person.variant = variant;
}

function isPersonInfected(person, variant) {
    return person.isInfected && person.variant === variant;
}

function infectA(data, isParentInfected = false) {
    for (let i = 0; i < data.length; i++) {
        let person = data[i];
        let isPreviousPersonInfected = i > 1 && isPersonInfected(data[i-1], VARIANT_A);
        if (person.isAlive) {
            if (isParentInfected || isPreviousPersonInfected) {
                infect(person, VARIANT_A);
            }
            if (person.group) {
                infectA(person.group, isPersonInfected(person, VARIANT_A));
            }
        }
    }
}

function infectB(data, isChildrenInfected = false) {
    for (let i = data.length - 1; i >= 0; i--) {
        let person = data[i];
        let isNextPersonInfected = i < data.length-1 && isPersonInfected(data[i+1], VARIANT_B);
        if (person.isAlive) {
            if (isChildrenInfected || isNextPersonInfected) {
                infect(person, VARIANT_B);
            }
            if (person.group) {
                infectB(person.group, isPersonInfected(person, VARIANT_B));
            }
        }
    }
}

function isChildrenInfected(person, variant) {
    if (!person.isAlive) {
        return false;
    }
    if (person.group) {
        return isPersonInfected(person, variant) || person.group.map(e => isChildrenInfected(e, variant)).find(e => e === true);
    }
    else {
        return isPersonInfected(person, variant);
    }
}

function infect32(data) {
    for (let person of data) {
        if (person.isAlive && !isPersonInfected(person, VARIANT_32) && person.age >= 32 && isChildrenInfected(person, VARIANT_32)) {
            infect(person, VARIANT_32);
        }
        if (person.group) {
            infect32(person.group);
        }
    }
}

function infectUltimate(data) {
    for (let person of data) {
        if (person.isAlive && !isPersonInfected(person, VARIANT_ULTIMATE) && isChildrenInfected(person, VARIANT_ULTIMATE)) {
            infect(person, VARIANT_ULTIMATE);
        }
    }
}

function cure(person, shouldImmunise = true) {
    person.isInfected = false;
    person.variant = VARIANT_NONE;
    if (shouldImmunise) {
        person.isImmunised = true;
    }
}

function vaccineA1(data) {
    for (let person of data) {
        if (person.isAlive && person.isInfected && (person.variant === VARIANT_A || person.variant === VARIANT_32) && person.age >= 0 && person.age <= 30) {
            cure(person);
        }
        if (person.group) {
            vaccineA1(person.group);
        }
    }
}

function vaccineB1(data, shouldKill = false) {
    for (let person of data) {
        if (person.isAlive && person.isInfected && (person.variant === VARIANT_B || person.variant === VARIANT_C)) {
            if (shouldKill) {
                person.isAlive = false;
            }
            else {
                cure(person, false);
            }
            shouldKill = !shouldKill;
        }
        if (person.group) {
            shouldKill = vaccineB1(person.group, shouldKill);
        }
    }
    return shouldKill;
}

function vaccineUltimate(data) {
    for (let person of data) {
        if (person.isAlive && person.isInfected && person.variant === VARIANT_ULTIMATE) {
            cure(person);
        }
    }
}

console.log("Avant infection");
displayData(data);

// Choisir infection(s) et/ou vaccin(s) en décommentant les lignes appropriées

// console.log("===============================");
// infectA(data);
// console.log("Après infection A");
// displayData(data);

// console.log("===============================");
// infectB(data);
// console.log("Après infection B");
// displayData(data);

// console.log("===============================");
// infect32(data);
// console.log("Après infection 32");
// displayData(data);

// console.log("===============================");
// infectUltimate(data);
// console.log("Après infection ultime");
// displayData(data);

// console.log("===============================");
// vaccineA1(data);
// console.log("Après vaccin A1");
// displayData(data);

// console.log("===============================");
// vaccineB1(data);
// console.log("Après vaccin B1");
// displayData(data);

// console.log("===============================");
// vaccineUltimate(data);
// console.log("Après vaccin ultime");
// displayData(data);